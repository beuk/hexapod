# HEXAPOD-ESP32
This is my COVID19-quarantine-anti-boredom hexapod project. The design is strongly based on: https://github.com/SmallpTsai/hexapod-v2-7697
Slight modifications are made wherever needed. 

## Electronics
The Linkit 7697 has been replaced by a ESP32 board (Wemos LOLIN D32). 
All custom PCB electronics have been replaced with breakout boards / custom through hole solder board.

Power is supplied by a 2s2p lithium ion battery pack. The voltage of the battery pack (7.4V) is converted to 5V for the servos by a single DC/DC converter.

### BOM:
- 1x Wemos LOLIN D32 (https://docs.wemos.cc/en/latest/d32/d32.html)
- 2x PCA9685 16 Channel PWM / Servo module (https://opencircuit.nl/Product/PCA9685-16-Channel-PWM-Servo-module-I2C)
- 18x SG92R servo (http://www.towerpro.com.tw/product/sg92r-7/)
- 1x Henge 12A UBEC DC/DC Converter (https://www.bitsandparts.nl/Converters/DC-DC-Converter-UBEC-Uin:7-25-5V-Uout:5-6-7-4V-12A-20A-max./p1883249)
- 4x Samsung INR18650-30Q battery (https://www.nkon.nl/samsung-inr-18650-30q-3000mah.html)
- 1x XT60 connector


## Software

### IDE
VSCode using PlatformIO.