#include <Comm.h>

namespace Comm {    
    char receivedMsg[numChars];
    char receivedToken;
    char tempChars[numChars];
    char foo;
    newDataType newData = NONE;
    std::map<char, pfunc> mapToken;
    std::function<void(char[])> funcMessage = _noop;

    void _noop(char msg[]) {}

    void receive() {
        static byte ndx = 0;
        char rc;

        while (Serial.available() > 0 && newData == NONE) {
            rc = Serial.read();
            Serial.print(rc);
            if (rc == endMarker) {
                receivedMsg[ndx] = '\0'; // terminate the string
                ndx = 0;
                newData = MESSAGE;
            }
            else if (mapToken.find(rc) != mapToken.end()) {
                receivedToken = rc;
                ndx = 0;
                newData = TOKEN;
            } 
            else {
                receivedMsg[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
        }
    }

    void handleData() {
        switch (newData) {
            case NONE:
                break;
            case MESSAGE:
                Log.trace("Received message: %s", receivedMsg);  
                funcMessage(receivedMsg);
                break;
            case TOKEN:
                Log.trace("Received token: %c", receivedToken);  
                pfunc f = mapToken[receivedToken];
                (*f)();
                break;
        } 
        newData = NONE;   
    }
}