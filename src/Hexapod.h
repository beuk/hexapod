#ifndef Hexapod_h
#define Hexapod_h

#include <Config.h>
#include <Servo.h>
#include <Pwm.h>
#include <Leg.h>
#include <Logger.h>

class Hexapod {
    public:
        Hexapod();
        void init();
        void moveLegs(Kinematics::Point &to);
        void moveLegs(Kinematics::PointArray &to);
        void moveLeg(int i, Kinematics::Point &to);
        
    Leg leg[N_LEGS];
    Pwm::Controller controller;
};

#endif
