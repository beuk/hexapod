#include <Config.h>

namespace Config {    
    const int servo[N_LEGS][N_JOINTS][4] = 
    {
        //  brd cha off dir
        {
            // RIGHT FRONT
            {1, 13,  -1,  1},
            {1, 14,  -9, -1},
            {1, 15, -11,  1}
        }, {
            // RIGHT CENTER
            {1, 4,  -1,  1},
            {1, 5,  -8, -1},
            {1, 6, -14,  1}
        }, {
            // RIGHT BACK
            {1, 2,   9,  1},
            {1, 1,  -2, -1},
            {1, 0,   0,  1}
        }, {
            // LEFT BACK
            {0, 13, -3,  1},
            {0, 14,  0,  1},
            {0, 15, -6, -1}
        }, {
            // LEFT CENTER
            {0, 4,  -2,  1},
            {0, 5,   4,  1},
            {0, 6,   2, -1}
        }, {
            // LEFT FRONT
            {0, 2,   2,  1},
            {0, 1,  -4,  1},
            {0, 0,  -5, -1}
        }
    }; 

    const int joint[N_JOINTS][2] = {
    //  low  upp
        {-45, 45},
        {-80, 80},
        {-45, 90}
    };

    const int board[N_BOARDS][2] = {
    //  addr   pwm_freq
        {0x40, 55},
        {0x41, 58}
    };
}
