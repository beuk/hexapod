#ifndef MotionTable_h
#define MotionTable_h

#include <Motion.h>
#include <Kinematics.h>

#define UP 20
#define FR 20

const Kinematics::PointArray standby_path[] = {
    {{  0,   0,   0},  {  0,   0,   0},  {  0,   0,   0},  {  0,   0,   0},  {  0,   0,   0},  {  0,   0,   0}}
};
const Kinematics::PointArray forward_path[] = {
    {{  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0}}, 
    {{ FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0}}, 
    {{  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP}}, 
    {{-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0}}
};
const Motion::MotionTable standby_table = {standby_path, 1, true};
const Motion::MotionTable forward_table = {forward_path, 4, true};
const Motion::MotionTable backward_table = {forward_path, 4, false};


#endif