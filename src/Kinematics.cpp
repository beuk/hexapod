#include <Kinematics.h>

namespace Kinematics {
    Point Point::operator-(const Point &b) {
        return Point(x_ - b.x_, y_ - b.y_, z_ - b.z_);
    };

    Point Point::operator+(const Point &b) {
        return Point(x_ + b.x_, y_ + b.y_, z_ + b.z_);
    };

    Point Point::operator*(const float& b) {
        return Point(x_ * b, y_ * b, z_ * b);
    };

    Point& Point::operator -=(const Point& b) {
        x_ -= b.x_;
        y_ -= b.y_;
        z_ -= b.z_;
        return *this;
    }

    Point& Point::operator +=(const Point& b) {
        x_ += b.x_;
        y_ += b.y_;
        z_ += b.z_;
        return *this;
    }

    bool Point::operator ==(const Point &b) {
        return (x_ == b.x_) && (y_ == b.y_) && (z_ == b.z_);
    }

    const Point& PointArray::get(int index) {
        return points_[index];
    }

    PointArray PointArray::operator-(const PointArray &b) {
        return PointArray{
            points_[0] - b.points_[0], points_[1] - b.points_[1], points_[2] - b.points_[2],
            points_[3] - b.points_[3], points_[4] - b.points_[4], points_[5] - b.points_[5],
        };
    }

    PointArray PointArray::operator*(const float& b) {
        return PointArray{
            points_[0] * b, points_[1] * b, points_[2] * b,
            points_[3] * b, points_[4] * b, points_[5] * b,
        };
    }

    PointArray& PointArray::operator +=(const PointArray& b) {
        for(auto i=0;i<6;i++)
            points_[i] += b.points_[i];
        return *this;
    }
}