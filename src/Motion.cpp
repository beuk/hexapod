#include <Motion.h>
#include <MotionTable.h>
namespace Motion {
    MotionTable motionLibrary[MOVEMENT_TOTAL] = {
        standby_table,
        forward_table,
        backward_table
    };
    Motion::Motion(MotionType type, int speed): speed_{speed}
    {
        motionTable = motionLibrary[type];
    }
         
    void Motion::next()
    {}
 
    MotionTable::MotionTable(const Kinematics::PointArray path[], int nSteps, bool isReversed): 
        nSteps_{nSteps}, isReversed_{isReversed}
    {}
    Kinematics::PointArray& MotionTable::getPathStep(int step)
    {}
}