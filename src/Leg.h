#ifndef Leg_h
#define Leg_h

#include <Logger.h>
#include <Servo.h>
#include <Kinematics.h>

class Leg
{
public:
    Leg();
    Leg(int index, const int jointConfig[N_JOINTS][2], const int servoConfig[N_JOINTS][4]);
    void init();
    void retract();
    void stretch();
    void setJointAngles(float angles[N_JOINTS]); 
    void setJointAngles(int a1, int a2, int a3);
    void moveTipLocal(Kinematics::Point &to);
    void moveTipGlobal(Kinematics::Point &to);
    void moveTipRelative(Kinematics::Point &to);
    Servo *servo[N_JOINTS];
private:
    int _index;
    int _mountAngle;
    Kinematics::Point _mountPoint;
    void globalToLocal(Kinematics::Point &global, Kinematics::Point &local);
};

#endif