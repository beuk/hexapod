#ifndef Config_h
#define Config_h

#define N_LEGS 6
#define N_JOINTS 3
#define N_SERVOS 18
#define N_BOARDS 2 

#define LEG_R0 38
#define LEG_R1 43
#define LEG_R2 92


namespace Config {    

    // (board, channel, offset, direction) 
    extern const int servo[N_LEGS][N_JOINTS][4];

    // (limit_low, limit_upp)
    extern const int joint[N_JOINTS][2];

    // (address, freq) 
    extern const int board[N_BOARDS][2];
}

#endif