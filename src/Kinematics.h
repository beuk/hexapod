#ifndef Kinematics_h
#define Kinematics_h

#include <Logger.h>

namespace Kinematics {    
    // A single point with x, y and z coordinates
    class Point {
    public:
        Point() = default;
        Point(float x, float y, float z): x_{x}, y_{y}, z_{z} {
        }
        Point operator-(const Point &b);
        Point operator+(const Point &b);
        Point operator*(const float& b);
        Point& operator -=(const Point& b);
        Point& operator +=(const Point& b);
        bool operator ==(const Point &b);
    public:
        float x_;
        float y_;
        float z_;
    };

    // A collection of 6 x, y, z points corresponding to the coordinates
    // of the hexapod tips
    class PointArray {
    public:
        PointArray() = default;
        PointArray(const Point& foreRight, const Point& right, const Point& hindRight, const Point& hindLeft, const Point& left, const Point& foreLeft):
            points_{foreRight, right, hindRight, hindLeft, left, foreLeft} {
        }

        const Point& get(int index);
        PointArray operator-(const PointArray &b);
        PointArray& operator +=(const PointArray& b);
        PointArray operator*(const float& b);
    private:
        Point points_[6];
    };    
}

#endif