#include <Leg.h>
#include <cmath>

Leg::Leg() {}

Leg::Leg(int index, const int jointConfig[N_JOINTS][2], const int servoConfig[N_JOINTS][4]): _index{index}
{    
    if (index <3)
        _mountAngle = (index + 1) * 45;
    else
        _mountAngle = (index + 2) * 45;    
    _mountPoint = Kinematics::Point(0, -LEG_R0-LEG_R1, LEG_R2);
    for(int i=0; i<N_JOINTS; i++) {
        servo[i] = new Servo(servoConfig[i]);   
        servo[i]->setLimits(jointConfig[i]);
    }
}

void Leg::init() {
    setJointAngles(0, 0, 0);
}

void Leg::retract() {
    setJointAngles(0, 80, -25);
}

void Leg::stretch() {
    setJointAngles(0, 80, 80);
}

void Leg::setJointAngles(float angles[N_JOINTS]) {    
    setJointAngles((int)angles[0], (int)angles[1], (int)angles[2]);
}

void Leg::setJointAngles(int a1, int a2, int a3) {    
    servo[0]->setAngle(a1);
    servo[1]->setAngle(a2);
    servo[2]->setAngle(a3);
}

const float pi = std::acos(-1);
const float hpi = pi/2;

// TODO: consider refactoring to predefined sin and cosine values (based on leg index)
void Leg::globalToLocal(Kinematics::Point &global, Kinematics::Point &local) {
    float ang = (float)_mountAngle / 180 * pi;
    local.x_ = global.x_ * std::sin(ang) - global.y_ * std::cos(ang);
    local.y_ = global.x_ * std::cos(ang) + global.y_ * std::sin(ang);
    local.z_ = global.z_;
}

void Leg::moveTipLocal(Kinematics::Point &to){
    Kinematics::Point relative = to - _mountPoint;
    Log.notice("Leg %d to relative tip position: %d, %d, %d", _index, (int)relative.x_, (int)relative.y_, (int)relative.z_);
    moveTipRelative(relative);
}

// TODO: combine with moveTipRelative()
void Leg::moveTipGlobal(Kinematics::Point &to) {
    Log.notice("Leg %d to global tip position: %d, %d, %d", _index, (int)to.x_, (int)to.y_, (int)to.z_);
    Kinematics::Point local;
    globalToLocal(to, local);
    Kinematics::Point relative = local - _mountPoint;
    moveTipRelative(relative);
}

// TODO: refactor to take absolute to Point
void Leg::moveTipRelative(Kinematics::Point &to){
    float angles[3];
    
    float x = to.x_;
    float y = to.y_;
    float y_ = std::sqrt(x*x + y*y) - LEG_R0;
    float z_ = to.z_;

    float c2 = y_*y_ + z_*z_;
    float c = std::sqrt(c2);
    float ar = std::atan2(z_, y_) * 180 / pi;
    float a1 = std::acos((c2 + LEG_R1*LEG_R1 - LEG_R2*LEG_R2)/(2*LEG_R1*c)) * 180 / pi;
    float at = std::acos((c2 - LEG_R1*LEG_R1 + LEG_R2*LEG_R2)/(2*LEG_R2*c)) * 180 / pi;

    angles[0] = std::atan2(to.x_, to.y_) * 180 / pi;
    angles[1] = ar + a1;
    angles[2] = 90 - (a1 + at);    
    
    Log.trace("joint angles: %d, %d, %d", (int)angles[0], (int)angles[1], (int)angles[2]);
    setJointAngles(angles);
}

