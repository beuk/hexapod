#include <Hexapod.h>

int i_board, i_leg, i_joint;

Hexapod::Hexapod() {}

void Hexapod::init() {   
    Log.notice("Initializing hexapod");
    for (i_board = 0; i_board < N_BOARDS; i_board++) 
        controller.board[i_board] = new Pwm::Board(Config::board[i_board]);

    for (i_leg = 0; i_leg < N_LEGS; i_leg++) {
        leg[i_leg] = Leg(i_leg, Config::joint, Config::servo[i_leg]);
        for (i_joint = 0; i_joint < N_JOINTS; i_joint++)
            controller.servo[i_leg*N_JOINTS + i_joint] = leg[i_leg].servo[i_joint];
    }    
    controller.begin();    
    controller.updateAll();
}

void Hexapod::moveLegs(Kinematics::Point &to) {
    for (i_leg = 0; i_leg < N_LEGS; i_leg++) {
        leg[i_leg].moveTipGlobal(to);
    }
    controller.updateAll();
}

void Hexapod::moveLegs(Kinematics::PointArray &to) {
    for (i_leg = 0; i_leg < N_LEGS; i_leg++) {
        Kinematics::Point p = to.get(i_leg);
        leg[i_leg].moveTipGlobal(p);
    }
    controller.updateAll();
}

void Hexapod::moveLeg(int i, Kinematics::Point &to) {
    leg[i].moveTipGlobal(to);
    controller.updateAll();
}