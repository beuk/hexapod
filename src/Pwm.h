#ifndef Pwm_h
#define Pwm_h

#include <Logger.h>
#include <Config.h>
#include <Servo.h>
#include <Adafruit_PWMServoDriver.h>

#define I2C_SPEED 1000000L  // 1MHz 

namespace Pwm {
    class Board: public Adafruit_PWMServoDriver
    {
    public:
        Board();
        Board(const int config[2]);
        void begin();
        void setPwmAll(uint16_t on, uint16_t off);
        void setPwmOn(uint8_t num, uint16_t on);
        void setPwmOff(uint8_t num, uint16_t off);
        void print();
    private:
        float _freq;
    };

    class Controller
    {
    public:
        Controller();
        void print();
        void begin();

        void updateServo(int iServo);
        void updateServo(Servo *s);
        void updateAll();

        // TODO: implement enable / disable via PCA9685 enable pin

        Board *board[N_BOARDS];
        Servo *servo[N_SERVOS];
    };    
}

#endif
