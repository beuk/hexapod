#include <Pwm.h>


namespace Pwm {

    int i;

    // CONTROLLER
    // =====================================================
    Controller::Controller() {}
    
    void Controller::print() {        
        Log.notice("");
        Log.notice("Im a Controller");
    }

    void Controller::begin() {
        for (i = 0; i < N_BOARDS; i++)
            board[i]->begin();
        delay(100);
    }

    void Controller::updateServo(Servo *s) {
        board[s->board]->setPwmOff(s->channel, s->getPulse());
    }
    
    void Controller::updateServo(int iServo) {
        Servo *s = servo[iServo];
        Log.verbose("set servo %d to: %d", iServo, (int)s->getAngle());
        updateServo(s);
    }
        
    void Controller::updateAll() {
        for (i = 0; i < N_SERVOS; i++)
            updateServo(i);
    }


    // BOARD
    // =====================================================
    Board::Board() {}

    Board::Board(const int config[2]) 
    : Adafruit_PWMServoDriver(config[0]), _freq(config[1])
    {}
    
    void Board::begin() {
        _i2c->begin(-1, -1, I2C_SPEED);
        reset();
        setOscillatorFrequency(FREQUENCY_OSCILLATOR);
        setPWMFreq(_freq);  // this also enables auto increment
        setPwmAll(0, 0);
    }

    void Board::setPwmAll(uint16_t on, uint16_t off) {
        _i2c->beginTransmission(_i2caddr);
        _i2c->write(PCA9685_ALLLED_ON_L);
        _i2c->write(on);
        _i2c->write(on >> 8);
        _i2c->write(off);
        _i2c->write(off >> 8);
        _i2c->endTransmission();
    }

    void Board::setPwmOn(uint8_t num, uint16_t on) {
        _i2c->beginTransmission(_i2caddr);
        _i2c->write(PCA9685_LED0_ON_L + 4 * num);
        _i2c->write(on);
        _i2c->write(on >> 8);
        _i2c->endTransmission();
    }

    void Board::setPwmOff(uint8_t num, uint16_t off) {
        _i2c->beginTransmission(_i2caddr);
        _i2c->write(PCA9685_LED0_OFF_L + 4 * num);
        _i2c->write(off);
        _i2c->write(off >> 8);
        _i2c->endTransmission();
    }

    void Board::print() {        
        Log.notice("");
        Log.notice("Im a Board");
    }    
}