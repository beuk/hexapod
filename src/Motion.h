#ifndef Motion_h
#define Motion_h

#include <Kinematics.h>

namespace Motion {
    enum MotionType {
        MOVEMENT_STANDBY,
        MOVEMENT_FORWARD,
        MOVEMENT_BACKWARD,
        MOVEMENT_TOTAL
    };

    class Motion {
        public:
            Motion() = default;
            Motion(MotionType type, int speed);
            void next();
        private:
            int speed_;
            int step_;
            MotionTable motionTable;
    };

    class MotionTable {
        public:
            MotionTable() = default;
            MotionTable(const Kinematics::PointArray path[], int nSteps, bool isReversed);
            Kinematics::PointArray& getPathStep(int step);

        private:
            const Kinematics::PointArray *path[];
            const int nSteps_;
            const bool isReversed_;
    };
}
#endif
