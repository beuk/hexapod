#include <Servo.h>

Servo::Servo() {}

Servo::Servo(const int config[4]) 
: board(config[0]), channel(config[1]), _dir(config[3]), _offset(config[2]), _angle(0), 
_limit_low(ANGLE_ABS_MIN), _limit_up(ANGLE_ABS_MAX)
{}

void Servo::setAngle(float angle) {
    // Always stay within (relative) angle limits
    if (angle < _limit_low) {
        angle = _limit_low;
        Log.warning("Reached servo %d-%d min angle: %d", board, channel, (int)_limit_low);
    }
    else if (angle > _limit_up) {
        angle = _limit_up;
        Log.warning("Reached servo %d-%d max angle: %d", board, channel, (int)_limit_up);
    }
    // Always stay within absolute angle limits
    int absAngle = _dir * (angle + _offset);
    if (absAngle < ANGLE_ABS_MIN) {
        _angle = _dir * ANGLE_ABS_MIN - _offset;
        Log.warning("Reached servo %d-%d min absolute angle: %d", board, channel, ANGLE_ABS_MIN);
    }
    else if (absAngle > ANGLE_ABS_MAX) {
        _angle = _dir * ANGLE_ABS_MAX - _offset;
        Log.warning("Reached servo %d-%d max absolute angle: %d", board, channel, ANGLE_ABS_MAX);
    }
    else
        _angle = angle;
}

void Servo::setLimits(const int limits[2]) {
    _limit_low = (float)limits[0];
    _limit_up = (float)limits[1];
}

void Servo::print() {        
    Log.notice("");
    Log.notice("Servo %d-%d", board, channel);
    Log.notice("angle: %d", (int)_angle);
    Log.notice("absAngle: %d", (int)getAbsAngle());
    Log.notice("pulse: %d", (int)getPulse());
}

float Servo::getAngle() {
    return _angle;
}

float Servo::getAbsAngle() {
    return _dir * (_angle + _offset);
}

int Servo::getPulse() {
    float divisor = (ANGLE_ABS_MAX - ANGLE_ABS_MIN);
    float pulse = (getAbsAngle() - ANGLE_ABS_MIN) * (PULSE_MAX - PULSE_MIN) / divisor + PULSE_MIN;
    return (int)pulse;
}
