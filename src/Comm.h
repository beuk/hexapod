#ifndef Comm_h
#define Comm_h

#include <Arduino.h>
#include <Logger.h>
#include <map>
#include <functional>  

namespace Comm {
    const char endMarker = '\r';
    const byte numChars = 32;
    enum newDataType {
        NONE = 0,
        MESSAGE = 1,
        TOKEN = 2
    };

    extern newDataType newData;
    extern char receivedMsg[numChars];
    extern char receivedToken;
    extern char tempChars[numChars];   

    // Map with tokens as keys and handler functions as values
    typedef void (*pfunc)();
    extern std::map<char, pfunc> mapToken;  

    // Function to handle message
    extern std::function<void(char[])> funcMessage;

    void _noop(char msg[]);
    void receive();
    void handleData();
}
    // NOTE: Keep below for decomposing message into parts
    // void handleString() {      // split the data into its parts
    //     char * strtokIndx; // this is used by strtok() as an index 
    //     strtokIndx = strtok(tempChars, VAL_SEP); // this continues where the previous call left off
    //     servoNumber = atoi(strtokIndx);     // convert this part to an integer
    // }
#endif
