#ifndef Logger_h
#define Logger_h

#include <ArduinoLog.h>

namespace Logger
{
    void begin(int baudRate, int level);
    void printTimestamp(Print* _logOutput);
    void printNewline(Print* _logOutput);
}
#endif