#include <Logger.h>

namespace Logger
{
    void begin(int baudRate, int level) {
        Serial.begin(baudRate);
        while(!Serial && !Serial.available()){}
        Log.begin(level, &Serial);
        Log.setPrefix(printTimestamp);
        Log.setSuffix(printNewline);
    }

    void printTimestamp(Print* _logOutput) {
    char c[12];
    sprintf(c, "%10lu ", millis());
    _logOutput->print(c);
    }

    void printNewline(Print* _logOutput) {
    _logOutput->print('\n');
    }
}