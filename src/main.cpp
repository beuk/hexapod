#include <Arduino.h>
#include <Hexapod.h>
#include <Logger.h>
#include <Comm.h>


// TODO: implement Movement class containing Position[x], delay, ?
// TODO: create MovementTable
// TODO: implement movements in hexapod

#define UP 20
#define FR 20

Hexapod hexapod;
Kinematics::PointArray l[] = {
    {{  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0}}, 
    {{ FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0}}, 
    {{  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP},  {  0,   0,   0},  {  0,   0,  UP}}, 
    {{-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0},  {-FR,   0,   0},  { FR,   0,   0}}
};
int i = 0;
bool enable = false;

void fwd() {            
    Log.notice("Moving forward");  
    enable = true;
};
void bwd() {            
    Log.notice("Moving backward");  
};
void left() {            
    Log.notice("Moving left"); 
};
void right() {             
    Log.notice("Moving right"); 
};
void halt() {             
    Log.notice("Halting"); 
    enable = false;
};

void setup() {
    Logger::begin(115200, LOG_LEVEL_NOTICE);
    Comm::mapToken['w'] = fwd;
    Comm::mapToken['x'] = bwd;
    Comm::mapToken['a'] = left;
    Comm::mapToken['d'] = right;
    Comm::mapToken['s'] = halt;    
    
    hexapod.init();
} 

void loop() {
    Comm::receive();
    Comm::handleData();    
    if (enable) {
        hexapod.moveLegs(l[i]);
        if (i >= 3) { 
            i = 0; 
        } else {
            i++;
        }
        delay(1000);
    }
}
