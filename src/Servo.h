#ifndef Servo_h
#define Servo_h

#include <Logger.h>
#include <Adafruit_PWMServoDriver.h>
#include <Config.h>

    
#define ANGLE_ABS_MIN -75
#define ANGLE_ABS_MAX 75
#define PULSE_MIN 180
#define PULSE_MAX 560

class Servo
{
public:
    Servo();
    Servo(const int config[4]);
    void print();
    void setAngle(float angle);
    void setLimits(const int limits[2]);
    float getAngle();
    float getAbsAngle();
    int getPulse();
    int board;
    int channel;
private:
    int _dir;
    float _offset;
    float _angle;
    float _limit_low;
    float _limit_up;
};
    
#endif